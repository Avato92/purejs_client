class Controller{
    /**This is Controller
     * @constructor 
     */
    constructor(){}

    /**
     * @function render
     * @param {*} route where print data
     * @param {*} template all the information
     */
    render(route, template){
        document.getElementById(route).innerHTML = template;
    }
}

export default Controller;