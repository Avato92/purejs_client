import MenuControler from '../src/controlers/menuCtrl';
import FooterControler from '../src/controlers/footerCtrl';
import CarouselControler from '../src/controlers/carouselCtrl.js';

jest.mock("../src/controlers/menuCtrl");
jest.mock("../src/controlers/footerCtrl");
jest.mock("../src/controlers/carouselCtrl");

test('Test render', () =>{
    MenuControler.render();
    expect(MenuControler.render).toHaveBeenCalledTimes(1);
    FooterControler.render();
    expect(FooterControler.render).toHaveBeenCalledTimes(1);
    CarouselControler.render();
    expect(CarouselControler.render).toHaveBeenCalledTimes(1);
});