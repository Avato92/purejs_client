import Controller from './Controller';

/**This is the CookiesControler 
 * @constructor
*/

class CookiesControler extends Controller {

    constructor(data, lang) {
        super();
        super.render('result', this.makeTemplate(data, lang));
    }
    makeTemplate(data, lang){
       

                /**
                 * @function filter
                 * we filter by languaje and key "Cookies"
                 */
                let txt = data.textos.filter((element) =>{
                    if(element.lang == lang){
                        return element;
                    }
                });
                let filterTxt = txt.filter((element) => {
                    if(element.key == "Cookies"){
                        return element;
                    }
                });
                return `
                <section class="body__text">
                    <h1 id="key">${filterTxt[0].key}</h1>
                    <p id="content">${filterTxt[0].content}</p>
                </section>
                `
    }
}

export default CookiesControler;