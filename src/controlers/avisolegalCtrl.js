import Controller from './Controller';
/**This is the AvisoLegalControler 
 * @constructor
*/

class AvisoLegalControler extends Controller{

    constructor(data, lang) {
       super(); 
       try{
            super.render('result', this.makeTemplate(data, lang));
       }catch(e){
           console.log(e);
       }
    }
     /** Function render of AboutUsCtrl */
    makeTemplate(data, lang){
                /**
                 * @function filter
                 * we filter by languaje and key "Legal Warning"
                 */
                let txt = data.textos.filter((element) =>{
                    if(element.lang == lang){
                        return element;
                    }
                });
                let filterTxt = txt.filter((element) => {
                    if(element.key == "Legal Warning"){
                        return element;
                    }
                });
                return `
                <section class="body__text">
                    <h1 id="key">${filterTxt[0].key}</h1>
                    <p id="content">${filterTxt[0].content}</p>
                </section>
                `
    }
}
/**Export to use in others files */
export default AvisoLegalControler;