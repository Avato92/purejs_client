#INTRODUCTION

Project by Alejandro Vañó, student of 2nd DAW

#Key points 
* Unit battery testing with jest
* Hot live reloading with webpack hmr
* Packaged with webpack
* Transpiled with babel
* Documented with jsdoc
* Routing
* Carousel
* Literal Templates system
* Catalogue
* Languaje

#Webpack

NICE TO READ https://www.valentinog.com/blog/webpack-tutorial/

#Ajax and fetch API 
NICE TO READ: https://dev.to/bjhaid_93/beginners-guide-to-fetching-data-with-ajax-fetch-api--asyncawait-3m1l

#TIPS
To solve some jest testing problems I've been forced to install

npm install babel-core@7.0.0-bridge.0 --save-dev

#SUMMARY
Project of the subject "Client" of the first quarter of second of DAW, with a server Django Rest Framework and client in JS