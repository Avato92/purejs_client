import {callServer} from '../src/utils.js';

const createMockXHR = (responseJSON) => {
    const mockXHR = {
        open: jest.fn(),
        send: jest.fn(),
        status: 200,
        response: JSON.stringify(
            responseJSON || {}
        )
    };
    return mockXHR;
}

describe('API integration test suite', function() {
    const oldXMLHttpRequest = window.XMLHttpRequest;
    let mockXHR = null;


    beforeEach(() => {
        mockXHR = createMockXHR();
        window.XMLHttpRequest = jest.fn(() => mockXHR);
    });

    afterEach(() => {
        window.XMLHttpRequest = oldXMLHttpRequest;
    });

    test('Should retrieve the list of posts from the server when calling getPosts method', function(done) {
        const reqPromise = callServer();
        mockXHR.response = [
                { title: 'test post' },
                { title: 'second test post' }
        ];
        mockXHR.onload();
        reqPromise.then((posts) => {
            expect(posts.length).toBe(2);
            expect(posts[0].title).toBe('test post');
            expect(posts[1].title).toBe('second test post');
            done();
        });
    });
});

