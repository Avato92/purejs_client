/**This is the MenuControler 
 * @constructor
*/

class MenuControler{

    /**
     * 
     * @param {*} data all information about enterprise
     */
    constructor(data){
        this.render(data);
    }

    render(data){
        /**
         *  @function render of MenuControler  
         * */
                let link = document.createElement('link');
                let head = document.getElementsByTagName('head').item(0);
                link.setAttribute('rel', 'icon');
                link.setAttribute('type','image/x-icon');
                document.title = data.name;
                document.getElementById("companyName").innerHTML = data.name;
                document.getElementById("logo").src = data.logo;
                link.setAttribute('href', data.icon_logo);
                document.getElementById('logo').setAttribute('alt', data.name);
                head.appendChild(link);
    }
}

export default MenuControler