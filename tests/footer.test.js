import {template} from '../src/templates/footer.js';
const $ = require('jquery');
const json = { facebook: "Hola", twitter: "Mundo"};

beforeEach(() =>{
    document.body.innerHTML = template(json);
});

test('First test, we check facebook and twitter `href` <a>',() => {
    expect($('#fb').attr('href')).toBe("Hola");
    expect($('#tw').attr('href')).toBe("Mundo");
});