import {template} from '../src/templates/cookies.js';
const $ = require('jquery');
const json = { key: "Cookies", content: "Lorem ipsum"}

beforeEach(() =>{
    document.body.innerHTML = template(json);
});

test('First test, we check key and content printed',() => {
    expect($('#key').text()).toBe("Cookies");
    expect($('#content').text()).toBe("Lorem ipsum");
});