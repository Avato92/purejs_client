import Controller from './Controller';
/**This is the ContactControler 
 * @constructor
*/

class ContactControler extends Controller {
    /**
     * 
     * @param {*} data information
     * @param {*} lang work in progress, to change languaje
     */
    constructor(data, lang) {
        super();
        try{
            super.render('result', this.makeTemplate(data));
            initMap(data);
        }catch(e){
            console.log(e);
        }
    }
  
    /** @function render of ContactControler  */
    makeTemplate(data) {
       return `
        <section class="body__content">
          <div class="body__content__box--primero">
            <div class="contact">
              <form class="contact_form" id="contact_form">
                <label>Name:</label><br />
                <input type="text" id="name" class="contact_form__name" required minlength="5" maxlength="20"/><br />
                <label>Email:</label><br />
                <input type="email" id="email" class="contact_form__email" required minlength="10" maxlength="50" pattern="^[a-z]{1}[A-Za-z0-9]{1,30}\@[a-z]{3,9}\.[a-z]{1,5}" /><br />
                <label>Reason:</label><br />
                <textarea type="text" id="reason" class="contact_form__textarea" required minlength="20" maxlength="200"></textarea><br />
                <input type="submit" id="sendEmail" class="contact_form__submit" value="Send">
              </form>
            </div>
          </div>
          <div class="body__content__box--segundo">
            <div id="map" class="map"></div>
          </div>
          <div class="body__content__box--tercero">
              <p id="phone">Telephone: ${data.phone}</p>
              <p id="zipcode">Postal code: ${data.zipcode}</p>
        </div>
      </section>`;    
    }
}
    /** @constructor 
     * @param {float} lat
     * @param {float} lon
     * @function initMap to start googleMaps
     */
    function initMap(data){
        /** Function to InitMap, Parse float, because google's parameter need numbers */
        let lat = parseFloat(data.location_lat);
        let long = parseFloat(data.location_long);
        let coord = {lat: lat, lng: long}; 
        /** Make infowindow */
        let contentInfoWindow = 
            `<div id="content">
                <h1 id="company_name">${data.name}</h1>
                <div id="bodyContent">
                    <p id="company_tel">Telephone: ${data.phone}</p>
                    <p id="city">${data.province},<br />
                    ${data.city},<br />
                    ${data.address}</p>
                </div>
            </div>
        `;
        /**Set coordinates
         * 
         * @param {lon} longitude on map. Must be a float value
         * @param {lat} latitude on map. Must be a float value
         * @param {zoom} zoom on map. Must be a integer
         */

        let map = new google.maps.Map(document.getElementById('map'), {
            zoom: 15,
            center: coord
        });
        /**Marker on map
         * @constructor
         * @param {lon}
         * @param {lat}
         * @param {title} pointName
         *   
         */
        let marker = new google.maps.Marker({
            position: coord,
            map: map,
            title: data.name
        });
        let infowindow = new google.maps.InfoWindow({
            content: contentInfoWindow
        }); 
        /** @event Listener to open infoWindow */
        marker.addListener('click', function() {
            infowindow.open(map, marker);
        });
    }
export default ContactControler;