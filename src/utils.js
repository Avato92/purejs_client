import {Settings} from './settings';

let CACHE_TEMPLATES = new Map();

  /** Add new parameter, because method can be GET, POST, PUT, DELETE
   * @param {string} url from server
   * @param {string} method 'GET', 'POST', 'PUT' ... default GET
   * @param {object} data can be empty
   */

function callServer(url, method = 'GET', data = 'No data') {
    // Return a new promise.
    return new Promise(function(resolve, reject) {
      if (CACHE_TEMPLATES.has(url)) {
        resolve(CACHE_TEMPLATES.get(url));
      }else{
        /** Do the usual XHR stuff */
        var req = new XMLHttpRequest();
        req.open(method, Settings.baseURL + url);

        req.onload = function() {
          console.log(req.status);
          /**This is called even on 404 etc
           so check the status */
          if (req.status == 200) {
            /**  Resolve the promise with the response text */
            CACHE_TEMPLATES.set(url,req.response);
            resolve(req.response);
          }
          else {
            /**  Otherwise reject with the status text
             which will hopefully be a meaningful error */
            reject(Error(req.statusText));
          }
        };
    
         /** Handle network errors */
        req.onerror = function() {
          reject(Error("Network Error"));
        };
    
         /** Make the request */
        if(data === 'No data'){
          req.send();
        }else{
          req.setRequestHeader('Accept', 'multipart/form-data');
          req.send(data);
        }
      }
      
    });
  }

  let slideIndex = 0;
  let timeOut;

  function plusSlides(n) {
    clearTimeout(timeOut);
    showSlides(slideIndex += n);
}

  // Thumbnail image controls
  function currentSlide(n) {
    showSlides(slideIndex = n);
}

  function showSlides() {
    let slides = document.getElementsByClassName("mySlides");
    for (let i = 0; i < slides.length; i++) {
        slides[i].style.display = "none";
    }
    slideIndex++;
    if (slideIndex > slides.length) {slideIndex = 1}
    slides[slideIndex-1].style.display = "block";
    timeOut = setTimeout(showSlides, 2000);
}

function setCookie(cName, cValue, exDays) {
  let d = new Date();
  d.setTime(d.getTime() + (exDays*24*60*60*1000));
  let expires = "expires="+ d.toUTCString();
  document.cookie = cName + "=" + cValue + ";" + expires + ";path=/";
}

function getCookie(cName) {
  let name = cName + "=";
  let decodedCookie = decodeURIComponent(document.cookie);
  let ca = decodedCookie.split(';');
  for(let i = 0; i <ca.length; i++) {
      let c = ca[i];
      while (c.charAt(0) == ' ') {
          c = c.substring(1);
      }
      if (c.indexOf(name) == 0) {
          return c.substring(name.length, c.length);
      }
  }
  return "";
}

function dontShowCarousel(){
  document.getElementById('carousel').innerHTML = "";
}
  

  export {callServer, showSlides, currentSlide, plusSlides, setCookie, getCookie, dontShowCarousel};