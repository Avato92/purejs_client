import {template} from '../src/templates/home.js';
const $ = require('jquery');
const json = {};
const json1 = [{
    nombretarifa : "Lion",
    precio : 2,
    codtarifa : 1
}];
const json2 = [{
    
    nombretarifa : "Turtle",
    precio : 3,
    codtarifa : 2
},
    {nombretarifa : "Rabbit",
    precio : 4,
    codtarifa : 3
}];
const json3 = [{

    nombretarifa : "Camel",
    precio : 5,
    codtarifa : 4
},
{
    nombretarifa : "Eagle",
    precio : 6,
    codtarifa : 5
},
{
    nombretarifa : "Wingull",
    precio : 7,
    codtarifa : 6
}];

test('No data test', () =>{
    document.body.innerHTML = template(json);
    expect($('#noData').text()).toBe("No rates");
});

test('Only 1 box', () =>{
    document.body.innerHTML = template(json1);
    expect($("#rateName").text()).toBe("Lion");
    expect($("#ratePrice").text()).toBe("2€/mes");
    expect($("#1").text()).toBe("Ver más");
});

test('Test two boxes', () =>{
    document.body.innerHTML = template(json2);
    expect($("#rateName1").text()).toBe("Turtle");
    expect($("#ratePrice1").text()).toBe("3€/mes");
    expect($("#2").text()).toBe("Ver más");
    expect($("#rateName2").text()).toBe("Rabbit");
    expect($("#ratePrice2").text()).toBe("4€/mes");
    expect($("#3").text()).toBe("Ver más");
}); 

test('Test three boxes', () =>{
    document.body.innerHTML = template(json3);
    expect($("#rateName1").text()).toBe("Camel");
    expect($("#ratePrice1").text()).toBe("5€/mes");
    expect($("#4").text()).toBe("Ver más");
    expect($("#rateName2").text()).toBe("Eagle");
    expect($("#ratePrice2").text()).toBe("6€/mes");
    expect($("#5").text()).toBe("Ver más");
    expect($("#rateName3").text()).toBe("Wingull");
    expect($("#ratePrice3").text()).toBe("7€/mes");
    expect($("#6").text()).toBe("Ver más");
})