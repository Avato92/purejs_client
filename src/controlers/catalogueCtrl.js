import Controller from './Controller';

/**
 * @constructor catalogueCtrl
 */

 class CatalogueControler extends Controller{
     constructor(data, filters, lang){
         super();
         super.render('result', this.makeTemplate(data.results, filters));
         document.getElementById('buttonFilter').addEventListener('click', function(){
            let cameraSelect = document.getElementById('camera').value;
            let screenSelect = document.getElementById('screen').value;
            let processorSelect = document.getElementById('processor').value;
            let ramSelect = document.getElementById('ram').value;
            let trademarkSelect = document.getElementById('trademark').value;
            let filtersSelected = {
                                    camera : cameraSelect,
                                    screen : screenSelect,
                                    processor : processorSelect,
                                    ram : ramSelect,
                                    trademark : trademarkSelect
                                };
            let templateFiltered = makeTemplateFiltered(filtersSelected, data.results);
            
         });
     }

     makeTemplate(data, filters){

        let camera = filters.camara.map((element) => {
            return `<option value="${element.id}">${element.num_camara}</option>`
        });
        let screen = filters.pantalla.map((element) => {
            return `<option value="${element.id}">${element.num_pantalla}</option>`
        });
        let tardemark = filters.marca.map((element) => {
            return `<option value="${element.id}">${element.Marca}</option>`
        });
        let processor = filters.procesador.map((element) => {
            return `<option value="${element.id}">${element.num_procesador}</option>`
        });
        let ram = filters.ram.map((element) => {
            return `<option value="${element.id}">${element.num_ram}</option>`
        });

        let phone = data.map((element) =>{
            return `
                    <div class="catalogue__container__product">
                        <img class="catalgue__container__product--img" src="${element.imagen}"><br />
                        <section class="catalogue__container__product--details">
                            <h3>${element.descripcion}</h3>
                            <p>${element.pvp}€</p>
                            <button class="catalogue__container__product--info">View More</button>
                        </section>
                    </div>
            `;
        });
        let template = `
        <div class="catalogue">
            <div class="catalogue__filters">
                <h1>Filters:</h1>
                <select id="camera" class="catalogue__filters--select">
                    <option value="0">Camera</option>
                    ${camera.join("")}
                </select>
                <select id="screen" class="catalogue__filters--select">
                    <option value="0">Screen</option>
                    ${screen.join("")}
                </select>
                <select id="trademark" class="catalogue__filters--select">
                    <option value="0">Tardemark</option>
                    ${tardemark.join("")}
                </select>
                <select id="processor" class="catalogue__filters--select">
                    <option value="0">Processor</option>
                    ${processor.join("")}
                </select>
                <select id="ram" class="catalogue__filters--select">
                    <option value="0">RAM</option>
                    ${ram.join("")}
                </select>
                <button class="catalogue__filters--button" id="buttonFilter">Filter</button>
            </div>
            <div class="catalogue__container">
                ${phone.join("")}
            </div>
        </div>
        `;
        return template;
     }


 }
 function makeTemplateFiltered(filters, data){

    if(filters.screen != 0){
        data = data.filter(function(element){
            return element.pantalla == filters.screen;
     });

    }if(filters.camera != 0){
        data = data.filter(function(element){
            return element.camara == filters.camera;
        });

    }if(filters.trademark != 0){
        data = data.filter(function(element){
            return element.marca == filters.tardemark;
        });

    }if(filters.ram != 0){
        data = data.filter(function(element){
            return element.ram == filters.ram;
        });

    }if(filters.processor != 0){
        data = data.filter(function(element){
            return element.procesador == filters.procesador;
        });

    }
     return data;
/*     let dataFiltered = data.results.filter((element) => {
        console.log(element);
        return ((element.camera == filters.camera) && 
                (element.screen == filters.screen) && 
                (element.ram == filters.ram) &&
                (element.trademark == filters.trademark) &&
                (element.processor == filters.processor));
    });
    console.log(dataFiltered); */
}


 export default CatalogueControler;