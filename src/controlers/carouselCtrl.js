import {showSlides} from '../utils';
import Controller from './Controller';

class CarouselControler extends Controller {

    /**This is the CarouselControler or Jumbotron
 * @constructor
*/

    constructor(data) {
        super();
        super.render('carousel', this.makeTemplate(data));
        showSlides();
    }
       /** Function render of CarouselCtrl */
    makeTemplate(data){
        /**We need this expresion to filter data from 'datos_empresa' */
        let expresion = /^jumbotron/i;

        
                let carouselItems = data.textos.filter((element) =>{return element.key.match(expresion);});
                
                let partialTemplate = "";
                /**we need a forEach to get all the information
                 * @param {object}
                 * @param {integer} loopNumber
                 */
                carouselItems.forEach(function(element,i){
                    
                    /**We start build the template with all the images and texts */
                partialTemplate += `
                    <div class="mySlides fade">
                        <div class="numbertext">${i+1} / ${carouselItems.length}</div>
                        <img id="carouselImg${i+1}" alt="${element.content}" src="${element.image}" style="width:100%">
                        <div id="carouselTxt${i+1}" class="text">${element.content}</div>
                    </div>
                    `;
                });
                /**When loops are finished, we put 'next' and 'previous' button */
                partialTemplate += `
                    <a class="prev" id="before">&#10094;</a>
                    <a class="next" id="next">&#10095;</a>
                `;
            
                partialTemplate += `<section id="circles" class="slidershow__circles">`;
            
                 for (let j = 0; j < carouselItems.length; j++){
                    partialTemplate += `<span class="dot" onclick="currentSlide(${j+1})"></span>`;
                } 
            
                partialTemplate += `</section>`;
            
                return partialTemplate;
    }
}

export default CarouselControler;