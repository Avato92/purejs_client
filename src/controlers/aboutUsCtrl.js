import Controller from './Controller';
/**This is the AboutUsControler 
 * @constructor
*/

class AboutUsControler extends Controller {
    /**
     * 
     * @param {*} data information to print
     * @param {*} lang we need this to filter and print
     */
    constructor(data, lang) {
        super();
        try{
            super.render('result', this.makeTemplate(data, lang));
        }catch(e){
            console.log(e);
        }
    }
    /** @function makeTemplate of AboutUsCtrl */
    makeTemplate(data, lang){

                /**
                 * @function filter
                 * we filter by languaje and key "About Us"
                 */
                let txt = data.textos.filter((element) =>{
                    if(element.lang == lang){
                        return element;
                    }
                });
                let filterTxt = txt.filter((element) => {
                    if(element.key == "About us"){
                        return element;
                    }
                });
                return `
                <section class="body__text">
                    <h1 id="key">${filterTxt[0].key}</h1>
                    <p id="content">${filterTxt[0].content}</p>
                </section>
                `
            
    }
}

export default AboutUsControler;