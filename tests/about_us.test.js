import {template} from '../src/templates/about_us.js';
const $ = require('jquery');
const json = { key: "Sobre nosotros", content: "Lorem ipsum"}

beforeEach(() =>{
    document.body.innerHTML = template(json);
});

test('First test, we check key and content printed',() => {
    expect($('#key').text()).toBe("Sobre nosotros");
    expect($('#content').text()).toBe("Lorem ipsum");
});