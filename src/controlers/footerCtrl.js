import Controller from './Controller';
/**This is the FooterControler 
 * @constructor
*/

class FooterControler extends Controller{

    constructor(data){
        super();
        try{
            super.render('footer', this.makeTemplate(data));
        }catch(e){
            console.log(e);
        }
    }
    makeTemplate(data){
        /** @function render of FooterControler  */

            return `
            <section class="footer__social">
                <h1 class="footer__social__title">Nos encontrarás:<h1>
                <a id="fb" href="${data.facebook}">
                    <img class="footer__social__button--fb" src="./css/fb_icon.png">
                </a>
                <a id="tw" href="${data.twitter}">
                    <img class="footer__social__button--tw" src="./css/tw_icon.png">
                </a>
            </section>
            <section class="footer__texts">
                <a class="footer__texts__links" href="#aviso_legal">Aviso legal</a>
                <span> | </span>
                <a class="footer__texts__links" href="#cookies">Cookies</a>
                <span> | </span>
                <a class="footer__texts__links" href="#about_us">Sobre nosotros</a>
            </section>
        `;
    }
}

export default FooterControler;