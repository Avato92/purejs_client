import {template} from '../src/templates/aviso_legal.js';
const $ = require('jquery');
const json = { key: "Aviso legal", content: "Lorem ipsum"}

beforeEach(() =>{
    document.body.innerHTML = template(json);
});

test('First test, we check key and content printed',() => {
    expect($('#key').text()).toBe("Aviso legal");
    expect($('#content').text()).toBe("Lorem ipsum");
});