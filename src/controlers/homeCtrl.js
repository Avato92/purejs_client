import Controller from './Controller';

/**This is the HomeControler 
 * @constructor
*/

class HomeControler extends Controller {
    /**
     * @param rates we get rates filtered
     * @event print homepage
     */
    constructor(rates) {
        super();
        try{
            let template = this.makeTemplate(rates);
            super.render('result', template);   
        }catch(e){
            console.log(e);
        }
    }

    makeTemplate(rates) {
         /**
          *  @function makeTemplate of HomeControler  
          * @param rates we make the template with 2 loops, rates and subrates
          * */
        try{
            const filteredRates = rates.results.map((element) => {
                const subRates = element.subtarifas.map((subElement) => {
                    /*>>>>>>>>>>>>>>>>>Information
                    tipo_tarifa == 1 = 'Movil'
                    tipo_tarifa == 2 = 'Fijo'
                    tipo_tarifa == 3 = 'Fibra'
                    tipo_tarifa == 4 = 'Wifi'
                    tipo_tarifa == 5 = 'TV'
                    <<<<<<<<<<<<<<<<<<<End Information*/
                    if(subElement.tipo_tarifa == 1){
                        return `<p>Mobile ${subElement.subtarifa_minutos_gratis} min/month<br/>${subElement.subtarifa_datos_internet} GB/month </p>`;
                    }else if(subElement.tipo_tarifa == 2){
                        return `<p>Land phone free calls</p>`;
                    }else if(subElement.tipo_tarifa == 3){
                        return `<p>Fiber optics ${subElement.subtarifa_velocidad_conexion_subida} MB/Upload <br/> ${subElement.subtarifa_velocidad_conexion_bajada} MB/Download</p>`
                    }else if(subElement.tipo_tarifa == 4){
                        return `<p>Wireless ${subElement.subtarifa_velocidad_conexion_subida} MB/Upload <br/> ${subElement.subtarifa_velocidad_conexion_bajada} MB/Download</p>`
                    }else if(subElement.tipo_tarifa == 5){
                        return `<p> TV ${subElement.subtarifa_num_canales} free channels</p>`
                    }else{
                        console.log("Error subtarifa");
                    }
                })
                const oneBox = `
                  <div class="box">
                    <article class="box__title" id="title_rate">
                      <h1 id="rateName">${element.nombretarifa}</h1>
                    </article>
                    <section class="box__option" id="paragraph">
                      ${subRates.join("")}
                      <p id="ratePrice">${element.precio}€/month</p>
                    </section>
                    <section class="box__button" id="section__button">
                    <button class="button__viewMore" id="${element.codtarifa}">Ver más</button>
                    </section>
                  </div>
              `
              return oneBox;
            });
    
            return `
            <div class="boxes">
                ${filteredRates.join("")}
            </div>`;
        }catch(e){
            console.log("error");
        };
    }
}
export default HomeControler;