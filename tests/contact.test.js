import {template} from '../src/templates/contact.js';
const $ = require('jquery');
const json = {phone : 123456, zipcode : 654321};

beforeEach(() =>{
    document.body.innerHTML = template(json);
});

test('Check contact form', () =>{
    expect($('#contact_form').length).toBeGreaterThan(0);
});

test('Check data', () =>{
    expect($("#phone").text()).toBe("Telephone: 123456");
    expect($("#zipcode").text()).toBe("Postal code: 654321");
});