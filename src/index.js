import {Router} from './router.js';
import HomeControler from './controlers/homeCtrl';
import ContactControler from './controlers/contactCtrl';
import MenuControler from './controlers/menuCtrl';
import FooterControler from './controlers/footerCtrl';
import AvisoLegalControler from './controlers/avisolegalCtrl';
import CookiesControler from './controlers/cookiesCtrl';
import AboutUsControler from './controlers/aboutUsCtrl';
import CarouselControler from './controlers/carouselCtrl';
import CatalogueControler from './controlers/catalogueCtrl';
import {callServer, getCookie, setCookie, dontShowCarousel} from './utils';

let languaje;

/**Router
 * @param {url}
 * List all the pages, if not found render home
 */

  Router
    .add(/contact/, function() {
      callServer('/datos_empresa').then(function(response){
        new ContactControler(JSON.parse(response), languaje);
        dontShowCarousel();
      }).catch(function(error) {
        console.log("Failed!", error);
      });
    }).listen()
/*     .add(/products\/(.*)\/edit\/(.*)/, function() {
        console.log('products', arguments);
    }) */
    .add(/aviso_legal/, function(){
      languaje = getCookie("lang");
      callServer('/datos_empresa').then(function(response){
        new AvisoLegalControler(JSON.parse(response), languaje);
        dontShowCarousel();
      }).catch(function(error) {
        console.log("Failed!", error);
      });
    }).listen()
    .add(/cookies/, function(){
      languaje = getCookie("lang");
      callServer('/datos_empresa').then(function(response){
        new CookiesControler(JSON.parse(response), languaje);
        dontShowCarousel();
      }).catch(function(error) {
        console.log("Failed!", error);
      });
    }).listen()
    .add(/about_us/, function(){
      languaje = getCookie("lang");
      callServer('/datos_empresa').then(function(response){
        new AboutUsControler(JSON.parse(response), languaje);
        dontShowCarousel();
      }).catch(function(error) {
        console.log("Failed!", error);
      });
    }).listen()
    .add(/catalogue/, function(){
      languaje = getCookie("lang");
      callServer('/articulo').then(function(response){
        callServer('/filtros').then(function(data){
          new CatalogueControler(JSON.parse(response),JSON.parse(data), languaje);
          dontShowCarousel();
        }).catch(function(error){
          console.log("Failed!", error);
        });
      }).catch(function(error){
        console.log("Failed!", error);
      });
    }).listen()
    .add(function() {
      callServer('/tarifa?destacado=true').then(function(response){
        new HomeControler(JSON.parse(response));
        callServer('/datos_empresa').then(function(data){
          new CarouselControler(JSON.parse(data));
        }).catch(function(error) {
          console.log("Failed!", error);
        });
      }).catch(function(error) {
          console.log("Failed!", error);
      });


    });



    /**
     * @event DOMContentLoaded
     * Set Cookies, if no exists is English
     *
     *  When page start render menu, footer, home and carousel 
     * */
    document.addEventListener("DOMContentLoaded", function(event) {
      languaje = getCookie("lang");
      if (!languaje){
        setCookie("lang", "en", 90);
      }

      /**
       * @function callServer to get all the information about enterprise
       */
      callServer('/datos_empresa').then(function(response){
        Router.navigate("home");
        new MenuControler(JSON.parse(response));
        new FooterControler(JSON.parse(response));
      });

      /**@event languajeButton to set languaje */
      document.getElementById('en').addEventListener('click', function(){
        setCookie("lang", "en", 90);
        location.reload();
      });
      document.getElementById('es').addEventListener('click', function(){
        setCookie("lang", "es", 90);
        location.reload();
      });
    });

